const express = require('express');
const router = express.Router();

const pole_controller = require('../controllers/pole.controller');

router.post('/create', pole_controller.pole_create);
router.get('/all', pole_controller.all_poles);
router.get('/:id', pole_controller.pole_by_id);

module.exports = router;