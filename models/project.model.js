const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let ProjectSchema = new Schema({
    name: {type: String},
    description: {type: String},
    pole_id: {type: Number}
});

module.exports = mongoose.model('Project', ProjectSchema);