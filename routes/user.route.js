const express = require('express');
const router = express.Router();

const user_controller = require('../controllers/user.controller');

//GET
router.get('/all', user_controller.all_users);
router.get('/:id', user_controller.user_info);

//POST
router.post('/create', user_controller.user_create);
router.post('/authentication', user_controller.authentication);

//PUT
router.put('/:id/update', user_controller.user_update);


module.exports = router;