const express = require("express");
const bodyParser = require("body-parser");
var mongoose = require('mongoose');
const user = require('./routes/user.route');
const feedback = require('./routes/feedback.route');
const pole = require('./routes/pole.route');
const role = require('./routes/role.route');
const project = require('./routes/project.route');
const app = express();
const errorHandler = require('./helpers/error-handler');
const cors = require('cors');

//MongoDB
mongoose.connect('mongodb://root:root@cluster0-shard-00-00-twgbi.mongodb.net:27017,cluster0-shard-00-01-twgbi.mongodb.net:27017,cluster0-shard-00-02-twgbi.mongodb.net:27017/octodev?ssl=true&replicaSet=Cluster0-shard-0&authSource=admin&retryWrites=true', { useNewUrlParser: true });

var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function() {
  console.log("connected")
});

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

//bodyParser
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());
app.use(cors());

//API path
app.use('/user', user);
app.use('/feedback', feedback);
app.use('/pole', pole);
app.use('/role', role);
app.use('/project', project);

app.use(errorHandler);


const HTTP_PORT = process.env.HTTP_PORT || 3001;
app.listen(HTTP_PORT, () => console.log(`Listening on port: ${HTTP_PORT}`));
