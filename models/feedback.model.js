const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let FeedbackSchema = new Schema({
    feeling: {type: String, required: true},
    successTasks: {type: String},
    difficulties: {type: String}
});

module.exports = mongoose.model('Feedback', FeedbackSchema);