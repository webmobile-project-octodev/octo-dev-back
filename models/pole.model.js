const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let PoleSchema = new Schema({
    id: {type: Number},
    name: {type: String}
});

module.exports = mongoose.model('Pole', PoleSchema);