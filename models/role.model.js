const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let RoleSchema = new Schema({
    id: {type: Number, required: true},
    name: {type: String, required: true}
});

module.exports = mongoose.model('Role', RoleSchema);