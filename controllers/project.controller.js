const Project = require('../models/project.model');

exports.project_create = function (req, res) {
    let project = new Project({
        name: req.body.name,
        description: req.body.name,
        pole_id: req.body.pole_id
    });

    project.save(function (err) {
        if (err) {
            return next(err);
        }
        res.send('Project Created successfully')
    });
};

exports.all_project = function (req, res) {
    Project.find({}, function(err, projects) {
        var projectMap = [];
    
        projects.forEach(function(project) {
            projectMap.push(project);
        });
    
        res.send(projectMap);  
    });
}

exports.projects_pole = function (req, res) {
    Project.find({pole_id: req.params.id}, function (err, projects) {
       
        if (err) return next(err);
        var projectMap = [];
    
        projects.forEach(function(project) {
            projectMap.push(project)
        });
        res.send(projectMap);
    })
}

exports.remove_duplicate_project = function (req, res) {
    var previousName = "";
    Project.find({}).sort({name: "asc"}).exec(function (err, projects) {
        if (err) return next(err);
        var projectMap = [];
    
        projects.forEach(function(project) {
            var name = project.name;
            if (name == previousName) {
                project.remove();
            }
            previousName = name;
        });
        res.send(projectMap);  
    });
}