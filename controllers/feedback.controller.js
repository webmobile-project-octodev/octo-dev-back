const Feedback = require('../models/feedback.model');

exports.feedback_create = function (req, res) {
    let feedback = new Feedback({
        feeling: req.body.feeling,
        successTasks: req.body.successTasks,
        difficulties: req.body.difficulties
    });

    feedback.save(function (err) {
        if (err) {
            return next(err);
        }
        res.send('Feedback Created successfully')
    });
};

exports.all_feedbacks = function (req, res) {
    Feedback.find({}, function(err, feedbacks) {
        var feedbackMap = [];
    
        feedbacks.forEach(function(feedback) {
            feedbackMap.push(feedback);
        });  
        res.send(feedbackMap);  
    });
}