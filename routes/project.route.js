const express = require('express');
const router = express.Router();

const project_controller = require('../controllers/project.controller');

router.post('/create', project_controller.project_create);

router.get('/all', project_controller.all_project);
router.get('/pole/:id', project_controller.projects_pole);

router.delete('/duplicate', project_controller.remove_duplicate_project);

module.exports = router;