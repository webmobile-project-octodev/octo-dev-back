const SHA256 = require("crypto-js/sha256");
var CryptoJS = require("crypto-js");

class Utils {

  static encrypt(data) {
    return CryptoJS.AES.encrypt(data, 'ynov-hash');
  }

  static decrypt(data) {
    var bytes  = CryptoJS.AES.decrypt(data.toString(), 'ynov-hash');
    return bytes.toString(CryptoJS.enc.Utf8);
  }

  // static hash(data) {
  //   return SHA256(JSON.stringify(data)).toString();
  // }

  // static decode(data) {
  //   return CryptoJS.HmacSHA1(data, "Key")
  // }
}

module.exports = Utils;
