const Pole = require('../models/pole.model');

exports.pole_create = function (req, res) {
    let pole = new Pole({
        name: req.body.name
    });

    pole.save(function (err) {
        if (err) {
            return next(err);
        }
        res.send('Pole Created successfully')
    });
};

exports.all_poles = function (req, res) {
    Pole.find({}, function(err, poles) {
        var poleMap = [];
    
        poles.forEach(function(pole) {
          poleMap.push(pole);
        });
    
        res.send(poleMap);  
    });
}

exports.pole_by_id = function (req, res) {
    Pole.findOne({id: req.params.id}, function (err, project) {
        if (err) return next(err);
        res.send(project);
    })
}