const Role = require('../models/role.model');

exports.role_create = function (req, res) {
    let role = new Role({
        id: req.body.id,
        name: req.body.name
    });

    role.save(function (err) {
        if (err) {
            return next(err);
        }
        res.send('Role Created successfully')
    });
};

exports.all_roles = function (req, res) {
    Role.find({}, function(err, roles) {
        var roleMap = [];
    
        roles.forEach(function(role) {
          roleMap.push(role);
        });
    
        res.send(roleMap);  
    });
}