const mongoose = require('mongoose');
const Schema = mongoose.Schema;
//const Role  = mongoose.model('./Role').schema;
var RoleSchema = require('./role.model').schema;
var PoleSchema = require('./pole.model').schema;
var ProjectSchema = require('./project.model').schema;

let UserSchema = new Schema({
    lastname: {type: String, required: true, max: 100},
    firstname: {type: String, required: true, max: 100},
    email: {type: String, required: true, max: 100},
    password: {type: String, required: true},
    role: {type: RoleSchema},
    pole: {type: PoleSchema},
    project: {type: ProjectSchema}
});

UserSchema.set('toJSON', { virtuals: true });

module.exports = mongoose.model('User', UserSchema);