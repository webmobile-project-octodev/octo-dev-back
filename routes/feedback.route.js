const express = require('express');
const router = express.Router();

const feedback_controller = require('../controllers/feedback.controller');

//GET
router.get('/all', feedback_controller.all_feedbacks);
//POST
router.post('/create', feedback_controller.feedback_create);

module.exports = router;