const express = require('express');
const router = express.Router();

const role_controller = require('../controllers/role.controller');

router.post('/create', role_controller.role_create);
router.get('/all', role_controller.all_roles);

module.exports = router;