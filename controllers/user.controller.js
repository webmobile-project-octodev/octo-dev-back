const User = require('../models/user.model');
const Utils = require('../utils');

exports.user_info = function (req, res, next) {
    User.findById(req.params.id, function (err, user) {
        if (err) return next(err);
        res.send(user);
    })
};

exports.user_create = function (req, res) {
    let newUser = new User(
        {
            lastname: req.body.lastname,
            firstname: req.body.firstname,
            email: req.body.email,
            password: Utils.encrypt(req.body.password),
            role: req.body.role,
            pole: req.body.pole,
            project: req.body.project
        }
    );
    
    User.findOne({email: newUser.email}, function(err, user, next){
        if (err) return next(err);
        if (user) {
            res.status(400).json({ message: 'Email already exist' });
          } else {
            newUser.save(function (err) {
                if (err) {            
                    return err;
                }
                res.status(200).json({ message: 'Account created successfully' });
            });
          }
    }) 
};

exports.user_update = function (req, res, next) {
    User.findByIdAndUpdate(req.params.id, {$set: req.body}, function (err, user) {
        if (err) return next(err);
        res.send(user);
    });
};

exports.all_users = function (req, res) {
    User.find({}, function(err, users) {
        var userMap = [];
    
        users.forEach(function(user) {
          userMap.push(user);
        });  
        res.send(userMap);  
    });
}

exports.authentication = function (req, res, next) {
    User.findOne({email: req.body.email}, function(err, user){
        if (err) return next(err);
        if (!user) {
            res.status(400).json({ message: 'Email or password is incorrect' });
          } else {
            if(Utils.decrypt(user.password) == req.body.password) {
                res.json(user)
            } else {
                res.status(400).json({ message: 'Email or password is incorrect' });
            }
        }
    });
}